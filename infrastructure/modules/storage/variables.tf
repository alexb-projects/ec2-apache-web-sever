variable "tags" {
  type        = map(string)
  description = "Tags associated to the resources"
}
variable "bucket_name" {
  type        = string
  description = "name of the bucket"
}