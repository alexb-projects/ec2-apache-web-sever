resource "random_string" "rand" {
  length  = 6
  special = false
  upper   = false
}

resource "aws_s3_bucket" "this" {
  bucket = "${var.bucket_name}-${random_string.rand.result}"

  tags = var.tags
}
