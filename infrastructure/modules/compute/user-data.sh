#!/bin/bash
sudo yum update -y
sudo yum install -y httpd

# create ec2admin user
sudo useradd -m ec2admin
sudo mkdir /home/ec2admin/.ssh
sudo echo "${public_key_1}" > /home/ec2admin/.ssh/authorized_keys

# create alexb user
sudo useradd -m alexb
sudo mkdir /home/alexb/.ssh
sudo echo "${public_key_2}" > /home/alexb/.ssh/authorized_keys

# overwrite default index.html page
cat >/var/www/html/index.html << EOF
<!DOCTYPE html>
<html>
  <style>
    h1 {
      text-align: center;
      font-size:60px;
      color:#590924
    }
    body {
      background-color: #092a5e;
    }
  </style>
  <body>
    <h1>${message_header}</h1>
    <p1>${message_body}</p1>
  </body>
</html>
EOF

sudo systemctl enable httpd
sudo systemctl start httpd

