data "template_file" "this" {
  template = file("./user-data.sh")

  vars = {
    message_header = "Test Website"
    message_body   = "Presented by Alex Bespalov"
    public_key_1   = file("./ssh-keys/vm-ssh.pub")
    public_key_2   = file("./ssh-keys/alexb-ssh.pub")
  }
}