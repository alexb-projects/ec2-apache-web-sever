resource "aws_security_group" "this" {
  name        = "${var.name_prefix}-sg"
  description = "Allow inbound / outbound traffic to the web-server"
  vpc_id      = var.vpc_id
  tags        = var.tags
}

resource "aws_security_group_rule" "ingress" {
  for_each = toset(var.ingress_ports)

  description       = "Allow inbound traffic"
  type              = "ingress"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = each.value
  to_port           = each.value
  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "egress" {
  for_each = toset(var.egress_ports)

  description       = "Allow outboud traffic. Required to download packages."
  type              = "egress"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = each.value
  to_port           = each.value
  security_group_id = aws_security_group.this.id
}

resource "aws_launch_configuration" "this" {
  name_prefix                 = "${var.name_prefix}-launch-config"
  instance_type               = var.instance_type
  image_id                    = var.image_id
  security_groups             = [aws_security_group.this.id]
  user_data                   = data.template_file.this.rendered
  associate_public_ip_address = true

  root_block_device {
    volume_size           = var.volume_size
    volume_type           = var.volume_type
    delete_on_termination = true
    encrypted             = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  name                      = "${var.name_prefix}-asg"
  launch_configuration      = aws_launch_configuration.this.name
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity
  health_check_type         = "EC2"
  health_check_grace_period = 600
  force_delete              = false
  vpc_zone_identifier       = var.subnets
  termination_policies      = ["OldestLaunchConfiguration", "Default"]

  depends_on = [aws_launch_configuration.this]

  lifecycle {
    create_before_destroy = true
  }
}