variable "region" {
  type        = string
  description = "Region where the AWS resources are provisioned"
}

variable "name_prefix" {
  type    = string
  default = "Prefix of the resource names"
}

variable "max_size" {
  type        = string
  description = "Maximus amount of instances in the web-server ASG"
  default     = "2"
}

variable "min_size" {
  type        = string
  description = "Minimum amount of instances in the web-server ASG"
  default     = "2"
}

variable "desired_capacity" {
  type        = string
  description = "Desired amount of instances in the web-server ASG"
  default     = "2"
}

variable "vpc_id" {
  type        = string
  description = "VPC id"
}

variable "subnets" {
  type        = list(string)
  description = "Subnets to which the EC2 instances will be deployed"
}

variable "tags" {
  type        = map(string)
  description = "Tags associated to the resources"
}

variable "ingress_ports" {
  type        = list(string)
  description = "List of allowed ports for the inboud connections"
}

variable "egress_ports" {
  type        = list(string)
  description = "List of allowed ports for the outboud connections"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
}

variable "image_id" {
  type        = string
  description = "Image ID"
}

variable "volume_size" {
  type        = string
  description = "volume size"
}

variable "volume_type" {
  type        = string
  description = "Volume type"
}