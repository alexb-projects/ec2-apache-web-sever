locals {
  aws_region          = "us-east-1"
  rg_name             = "north-america-1"
  sa_name             = "alex-b-dev"
  vpc_cidr            = "10.0.0.0/16"
  public_subnet_cidrs = ["10.0.1.0/24"]
}
