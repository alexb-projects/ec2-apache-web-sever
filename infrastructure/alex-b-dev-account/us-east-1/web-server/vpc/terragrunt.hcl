locals {
  module_source_url = "tfr:///terraform-aws-modules/vpc/aws"
  module_version    = "5.1.2"
  tags = {
    rg-name   = include.root.inputs.rg_name
    sa-name   = include.root.inputs.sa_name
    org-name  = include.root.inputs.org_name
    dept-name = include.root.inputs.dept_name
  }
}

include "root" {
  path   = find_in_parent_folders("common_config.hcl")
  expose = true
}

terraform {
  source = "${local.module_source_url}//?version=${local.module_version}"
}

inputs = {
  name                 = "${include.root.inputs.rg_name}-vpc"
  cidr                 = include.root.inputs.vpc_cidr
  azs                  = ["${include.root.inputs.aws_region}a"]
  public_subnets       = include.root.inputs.public_subnet_cidrs
  enable_dns_hostnames = true

  default_vpc_tags = local.tags
}