locals {
  module_source_url = "${get_repo_root()}//infrastructure/modules/compute"

  tags = {
    rg-name   = include.root.inputs.rg_name
    sa-name   = include.root.inputs.sa_name
    org-name  = include.root.inputs.org_name
    dept-name = include.root.inputs.dept_name
  }
}

include "root" {
  path   = find_in_parent_folders("common_config.hcl")
  expose = true
}

terraform {
  source = local.module_source_url
}

dependency "vpc" {
  config_path = "../vpc"
}

inputs = {
  region        = include.root.inputs.aws_region
  name_prefix   = "${include.root.inputs.sa_name}-web-server"
  vpc_id        = dependency.vpc.outputs.vpc_id
  subnets       = dependency.vpc.outputs.public_subnets
  ingress_ports = ["22", "80", "443", "8080"]
  egress_ports  = ["443"]
  instance_type = "t2.micro"
  image_id      = "ami-041feb57c611358bd"
  volume_size   = "16"
  volume_type   = "gp3"
  tags          = local.tags
}
