locals {
  module_source_url = "${get_repo_root()}//infrastructure/modules/storage"

  tags = {
    rg-name   = include.root.inputs.rg_name
    sa-name   = include.root.inputs.sa_name
    org-name  = include.root.inputs.org_name
    dept-name = include.root.inputs.dept_name
  }
}

include "root" {
  path   = find_in_parent_folders("common_config.hcl")
  expose = true
}

terraform {
  source = local.module_source_url
}

inputs = {
  bucket_name = "${include.root.inputs.org_name}-${include.root.inputs.dept_name}-${include.root.inputs.account_name}"
  tags        = local.tags
}