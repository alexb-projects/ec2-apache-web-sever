To build the docker image:
  1. navigate to the directory with the Docerfile
  2. execute: 
        docker build -t lab-interview:0.0.1 .

To run the image, execute:
  docker run --name lab-interview lab-interview:0.0.1

To upload it to the dockerhub execute the following commands:

  1. docker login
  2. docker tag lab-interview:0.0.1 <USERNAME>/<REPOSITORY>:0.0.1
  3. docker push <USERNAME>/<REPOSITORY>:0.0.1
